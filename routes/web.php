<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('trangchu',function()
{
	return view('layouts.index');
});
Route::group(['prefix'=>'/','middleware'=>'admin_login'],function()
{
	Route::resource('user','UserController');
	Route::resource('projects','ProjectController');
	
});
Route::post('login','LoginController@check');
Route::get('login','LoginController@index');
Route::get('logout','LoginController@destroy');

