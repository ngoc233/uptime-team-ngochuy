<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="#"><i class="fa fa-dashboard"></i>DashBoard<span class="fa fa-chevron-down"></span></a>
      </li>
      <li><a><i class="fa fa-user"></i>User <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{url('user')}}">List</a></li>
          <li><a href="{{url('user/create')}}">Create</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-user"></i>Project <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{url('projects')}}">List</a></li>
          <li><a href="{{url('projects/create')}}">Create</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>