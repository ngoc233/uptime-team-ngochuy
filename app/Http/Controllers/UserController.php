<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use App\User;
use Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('users.index',['user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        DB::beginTransaction();
        try{
            $message = [
               'name.required'               =>   'Trường này không được phép để trống',
                'name.max'                   =>   'Tên không được vượt qua 255 ký tự',
                'name.min'                   =>   'Tên không ít hơn 3 ký tự',
                'email.required'             =>   'Trường email không được phép để trống',
                'email.unique'               =>   'Trường email đã tồn tại ',
                'password.required'          =>   'Trường password không được phép để trống',
                'password.min'               =>   'Trường password không được ít hơn 3 ký tự',
                'password.max'               =>   'Trường password không được vượt qua 255 ký tự',
                'phone.required'             =>   'Trường phone không được phép để trống',
                'phone.min'                  =>   'Phone không ít hơn 3 ký tự',
                'phone.max'                  =>   'Phone không vượt quá 15 ký tự',
                'sort_order.required'        =>   'Trường này không được để trống',
                
            ];
            $validator =  Validator::make($data, [
                'name'          =>  'required|min:3|max:255',
                'email'         =>  'required|unique:users,email|max:255|email',
                'password'      =>  'required|min:3|max:255',
                'phone'         =>  'required|min:3|max:15',
                'sort_order'   =>   'required',
            ],$message);
            if($validator->fails()){
                return redirect()->back()->withInput($data)->withErrors($validator);
            }
            $data['password'] = bcrypt($request->password);
            User::create($data);
            DB::commit();
            Session()->flash('success', 'Succsess !! Complete Add Project');
            return Redirect(url('user'));
        } catch(Exception $e){
            DB::rollback();
            response()->json([
                    'error' => true,
                    'message' => 'Internal Server Error'
                ], 500);
        }
    }
    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =  User::find($id);
        return view('users.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        unset($data['_token']);
        DB::beginTransaction();
        try{
            $message = [
                'name.required'               =>   'Trường này không được phép để trống',
                'name.max'                   =>   'Tên không được vượt qua 255 ký tự',
                'name.min'                   =>   'Tên không ít hơn 3 ký tự',
                'email.required'             =>   'Trường email không được phép để trống',
                'password.required'          =>   'Trường password không được phép để trống',
                'password.min'               =>   'Trường password không được ít hơn 3 ký tự',
                'password.max'               =>   'Trường password không được vượt qua 255 ký tự',
                'phone.required'             =>   'Trường phone không được phép để trống',
                'phone.min'                  =>   'Phone không ít hơn 3 ký tự',
                'phone.max'                  =>   'Phone không vượt quá 15 ký tự',
                'sort_order.required'        =>   'Trường này không được để trống',
                'sort_order.max'             =>   'Sort_order không vượt quá 225 ký tự',
            ];
            $validator =  Validator::make($data, [
                'name'          =>  'required|min:3|max:255',
                'email'         =>  'required|max:255|email',
                'password'      =>  'required|min:3|max:255',
                'phone'         =>  'required|min:3|max:15',
                'sort_order'   =>   'required|max:15',
            ],$message);
            if($validator->fails())
            {
                return redirect()->back()->withInput($data)->withErrors($validator);
            }
            $user = User::find($id)->update($data);;
            DB::commit();
            Session()->flash('success', 'Succsess !! Complete Add Project');
            return redirect(url('user'));
        } catch(Exception $e){
            Log::info('sua không thành công');
            DB::rollback();
            response()->json([
                    'error' => true,
                    'message' => 'Internal Server Error'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect(url('user'))->with('thongbao','Xóa thành công');

    }
   

}
